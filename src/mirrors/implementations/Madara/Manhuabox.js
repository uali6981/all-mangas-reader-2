if (typeof registerMangaObject === 'function') {
    registerMangaObject({
        mirrorName: "Manhwafull",
        mirrorIcon: "manhwafull.png",
        languages: "en",
        domains: ["manhwafull.com"],
        home: "https://manhwafull.com/",
        chapter_url: /^\/manga\/.+\/.+$/g,
        canListFullMangas: false,
        abstract: "Madara",
        abstract_options: {
            search_url: "https://manhwafull.com/",
            img_src: "data-src",
            chapter_list_ajax: true
        }
    })
}

